# This file is a template, and might need editing before it works on your project.
FROM node
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app
RUN npm cache verify
RUN npm install --verbose
COPY . /usr/src/app
EXPOSE 3000
CMD ["npm","start"]
#RUN npm start
